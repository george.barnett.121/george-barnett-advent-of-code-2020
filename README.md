# George Barnett's Advent of Code 2020

These are my attempts at advent of code 2020 from here: https://adventofcode.com/2020

I'll try to only push on the days after the puzzle is released, but I might also use this to transfer between computers, so be warned with spoilers!

## To use

copy the git repository
```
git clone https://gitlab.com/george.barnett.121/george-barnett-advent-of-code-2020.git
```


Copy each day's data into the data folder (../aoc/data/) as 'day_1.txt', 'day_2.txt', etc
install the aoc pip repository by using 
```
pip install -e .
```

In your puzzles you can import aoc and the data using this:

```
from aoc import read_puzzle_inputs
puzzle_input = read_puzzle_inputs(day=1)
```

If you want the outputs in string format (default format is int) use 
```
puzzle_input = read_puzzle_inputs(day=1,str)
```