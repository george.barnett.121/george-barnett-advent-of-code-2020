import pkgutil

def read_puzzle_inputs(day:int,output_type=int):
    try:
        puzzle_input = pkgutil.get_data('aoc.data', f'day_{day}.txt').splitlines()
    except FileNotFoundError:
        raise FileNotFoundError(f"Day {day} inputs have gone missing. Please download day 1 puzzle inputs  and save them to aoc/data/day_1.txt")
    return [output_type(entry.decode("ascii")) for entry in puzzle_input]