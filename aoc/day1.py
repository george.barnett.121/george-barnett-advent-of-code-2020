from aoc import read_puzzle_inputs

puzzle_input = read_puzzle_inputs(day=1)

""" Part 1
Find two entries that sum to 2020 then multiply them together
"""
def part_1(inputs,num_to_sum=2020) -> int:

    return output

test_inputs = [1721,979,366,299,675,1456]
part_1_example_answer = 514579
assert part_1_example_answer == part_1(test_inputs)
test_answer = part_1(test_inputs)

real_answer = part_1(puzzle_input)


""" Part 2
Find 3 entries that sum to 2020
"""

def part_2(inputs,num_to_sum=2020) -> input:
    
    return output

part_2_example_answer = 241861950
assert part_2_example_answer == part_2(test_inputs)

part_2_real_answer = part_2(puzzle_input)
